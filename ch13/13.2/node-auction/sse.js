const SSE = require('sse');

module.exports = (server) => {
  const sse = new SSE(server);
  sse.on('connection', (client) => { // connection 이벤트 리스너에 연결하여 
                                     // 클라이언트와 연결할 때 어떤 동작을 할지 정의
    setInterval(() => {
      client.send(Date.now().toString()); // 1초마다 서버 시간 타임스탬프롤 보내도록 함
    }, 1000);
  });
};
